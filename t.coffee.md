Generic operations on streams

    map = (f) -> (S) ->
      for await x from S
        yield f x
      return

    filter = (f) -> (S) ->
      for await x from S when f x
        yield x
      return

    forEach = (f) -> (S) ->
      for await x from S
        f x
      return

    first = (n) -> (S) ->
      for await x from S
        return unless n--
        yield x
      return

    last = (S) ->
      for await x from S
        null
      yield x
      return

    skip = (n) -> (S) ->
      for await x from S
        if n
          n--
        else
          yield x
      return

    accumulate = (a,f) -> (S) ->
      for await x from S
        yield a = f a, x
      return

    sleep = (t) -> new Promise (resolve) -> setTimeout resolve, t

    delay = (t) -> (S) ->
      for await x from S
        await sleep t
        yield x
      return

    nice = (S) ->
      for await x from S
        yield x
        await nexttick()
      return

    dup = (S) ->
      for await x from S
        yield x
        yield x
      return

    every = (n) -> (S) ->
      c = n
      for await x from S
        if c
          c--
        else
          c = n
          yield x
      return

    _while = (f) -> (S) ->
      for await x from S
        return unless f x
        yield x
      return

    byline = (S) ->
      buf = ''
      for await x from S
        buf += x
        while 0 <= i = buf.indexOf '\n'
          yield buf.substring 0, i
          buf = buf.substring i+1
      return

    EventEmitter = require 'events'

    span = (S) ->
      ev = new EventEmitter

      running = true
      backlogged = 0
      ev.once 'start', ->
        for await value from S
          await sleep backlogged/100 if backlogged > 0
          return if not running
          ev.emit 'data', value
        ev.emit 'end'
        return

      [Symbol.asyncIterator]: ->
        backlog = []
        done = false
        ev.on 'data', (value) ->
          backlog.push value
          backlogged++
          return
        ev.once 'end', ->
          done = true
          return
        next: ->
          backlogged--
          switch
            when backlog.length > 0
              Promise.resolve value:backlog.shift(),done:false
            when done or not running
              Promise.resolve done:true
            else
              new Promise (resolve) ->
                on_data = ->
                  ev.off 'end', on_end
                  resolve value:backlog.shift(),done:false
                  return
                on_end = ->
                  ev.off 'data', on_data
                  resolve done:true
                  return

                ev.once 'data', on_data
                ev.once 'end', on_end
                return

      close: ->
        running = false
        ev.removeAllListeners()
        return
      start: ->
        ev.emit 'start'
        return

    nexttick = -> new Promise process.nextTick

Build an async stream from a sync stream/iterator

    sync_to_async = (S) ->
      for x from S
        yield x
        await nexttick()
      return

Return an async iterator from eithe a sync or async stream.

    async_iterator_of = (S) ->
      switch
        when Symbol.asyncIterator of S
          return S[Symbol.asyncIterator]()
        when Symbol.iterator of S
          return sync_to_async S[Symbol.iterator]()
        else
          throw new Error 'Argument must be an iterator or an async iterator'

Merge two streams (async or sync)

    merge = (S1,S2) ->
      s1 = async_iterator_of S1
      s2 = async_iterator_of S2

      done_s1 = false
      done_s2 = false
      has_value_s1 = false
      has_value_s2 = false
      value_s1 = null
      value_s2 = null

      [Symbol.asyncIterator]: ->
        next: ->
          switch
            when has_value_s1
              has_value_s1 = false
              value = value_s1
              value_s1 = null
              Promise.resolve {value,done:false}
            when has_value_s2
              has_value_s2 = false
              value = value_s2
              value_s2 = null
              Promise.resolve {value,done:false}
            when done_s1 and done_s2
              Promise.resolve done: true
            when done_s1
              s2.next()
            when done_s2
              s1.next()
            else

              new Promise (resolve) ->
                resolved = false
                next_s1 = ({value,done}) ->
                  done_s1 = done
                  if done
                    if done_s2
                      resolved = true
                      resolve done:true
                    # Otherwise, keep pulling from s2
                    return
                  if resolved # by s2
                    has_value_s1 = true
                    value_s1 = value
                  else
                    resolved = true
                    resolve {value,done:false}
                  return
                next_s2 = ({value,done}) ->
                  done_s2 = done
                  if done
                    if done_s1
                      resolved = true
                      resolve done:true
                    # Otherwise, keep pulling from s1
                    return
                  if resolved # by s1
                    has_value_s2 = true
                    value_s2 = value
                  else
                    resolved = true
                    resolve {value,done:false}
                s1.next().then next_s1
                s2.next().then next_s2
                return

    concat = (S...) ->
      for s in S
        for await x from s
          yield x
      return

    log = forEach (x) -> console.log x

Natural

    Zero = `0n`
    One = `1n`
    Two = `2n`

    Natural = ->
      n = Zero
      loop
        yield n++
      return

    __neg = (x) -> -x

    Integer = ->
      merge Natural(),
            (map __neg) (skip 1) Natural()

    Even = -> (map (x) -> x*Two) Natural()
    Odd  = -> (map (x) -> x*Two+One) Natural()
    Cubes = -> (map (x) -> x*x*x) Natural()

    Even2 = -> (filter (x) -> x%Two is Zero) Natural()
    Odd2  = -> (filter (x) -> x%Two is One) Natural()

    sum = (S) -> last (accumulate Zero, (a,x) -> a+x) S
    count = (S) -> last (accumulate Zero, (a,x) -> a+One) S
    count2 = (S) -> sum (map -> One) S

    fs = require 'fs'
    do ->
      try
        console.log 'Merging async'
        await log (first 20) merge ((skip 5) (delay 10) Even()), ((delay 7) Odd())
        console.log 'Merging sync'
        await log (first 20) merge ((skip 5) Even()), Odd()
        console.log 'Handling error'
        try
          await log (first 20) [Symbol.asyncIterator]: -> next: -> throw new Error 'on purpose'
        catch error
          console.log '(Caught error)', error
        console.log 'File'
        await log (first 5) (skip 10) byline fs.createReadStream './t.coffee.md', 'utf-8'
        console.log 'Naturals and file'
        await log (delay 0) (first 200) merge (Natural()), (byline fs.createReadStream './t.coffee.md', 'utf-8')
        console.log 'Integers and file'
        await log (delay 0) (first 200) merge (Integer()), (byline fs.createReadStream './t.coffee.md', 'utf-8')
        console.log 'Counting lines'
        console.time 'count1'
        lines = -> (byline fs.createReadStream './t.coffee.md', 'utf-8')
        await log last (accumulate Zero, (a,x) -> a+x) (map (l) -> One) lines()
        await log sum (map (l) -> One) lines()
        await log count lines()
        await log count2 lines()
        console.timeEnd 'count1'
        console.log 'Countings lines from span.'
        console.time 'count2'
        lines = span (fs.createReadStream './t.coffee.md', 'utf-8')
        do ->
          await Promise.all [
            log last (accumulate Zero, (a,x) -> a+x) (map (l) -> One) byline lines
            log sum (map (l) -> One) byline lines
            log count byline lines
            log count2 byline lines
          ]
          lines.close()
          console.timeEnd 'count2'
        lines.start()

      catch error
        console.error error
      console.log 'Done.'
